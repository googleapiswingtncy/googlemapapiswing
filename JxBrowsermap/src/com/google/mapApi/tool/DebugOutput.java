package com.google.mapApi.tool;

import static org.fusesource.jansi.Ansi.ansi;

import java.io.OutputStream;
import java.io.PrintStream;

import org.fusesource.jansi.AnsiConsole;
import org.fusesource.jansi.Ansi.Color;

/**
 * @author Arnault Le Pr�vost-Corvellec
 *
 */

public class DebugOutput {
	private static boolean enable = true;
	private static PrintStream ps = System.out;
	
	/** Active ou desative le mode Debugage
	 * @param enable
	 */
	public static void setEnable(boolean enable) {
		DebugOutput.enable = enable;
	}

	/** Affiche les information de debug si il est activ�
	 * @param o
	 */
	public static void println(Object o) {
		if (enable)
			ps.println(ansi().fg(Color.CYAN).a(o).reset());
	}
	public static void setDebugTo(OutputStream output){
		ps = new PrintStream(output);		
	}
	public static void init(){
		System.setProperty("jansi.passthrough", "true");
		AnsiConsole.systemInstall();
		enable=true;
	}
	public static void close(){
		AnsiConsole.systemUninstall();
	}
	
}
