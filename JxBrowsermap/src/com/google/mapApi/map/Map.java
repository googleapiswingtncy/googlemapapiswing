package com.google.mapApi.map;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.google.mapApi.map.addon.drawer.DrawerTool;
import com.google.mapApi.map.addon.drawer.marker.Marker;
import com.google.mapApi.map.addon.drawer.marker.MarkerList;
import com.google.mapApi.map.addon.drawer.marker.MarkerTool;
import com.google.mapApi.map.addon.drawer.polygone.Polygone;
import com.google.mapApi.map.addon.drawer.polygone.PolygoneList;
import com.google.mapApi.map.addon.drawer.polyline.Polyline;
import com.google.mapApi.map.addon.drawer.polyline.PolylineList;
import com.google.mapApi.map.addon.searchTool.SearchTool;
import com.google.mapApi.map.context.GoogleApiContext;
import com.google.mapApi.map.context.NoAPIKeyDefineException;
import com.google.mapApi.tool.DebugOutput;
import com.google.maps.model.Bounds;
import com.google.maps.model.LatLng;

/**
 * Composant de type panel affichant une carte dynamique de google map
 * 
 * This project and all these classes can be used without restriction for open
 * source or academic applications as part of projects with pedagogical purpose.
 * It remains the full property of its contributors and any use by enterprises
 * or companies need a written agreement between the contributors and the said
 * company.
 * 
 * 
 * @author Arnault Le Pr�vost-Corvellec
 *
 */
public class Map extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6533347272597199956L;
	private Browser browser;
	private SearchTool st;
	private MarkerTool ma;
	private GridBagConstraints c;
	private MarkerList ml;
	private DrawerTool dt;
	private PolylineList pll;
	private PolygoneList plg;

	/**
	 * Constructeur avec mot de passe du keystore
	 * 
	 * @param password
	 * @throws NoAPIKeyDefineException
	 */
	public Map(String password) throws NoAPIKeyDefineException {
		super(new GridBagLayout());
		ml = new MarkerList();
		pll = new PolylineList();
		plg = new PolygoneList();

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.weighty = 1;
		browser = new Browser();
		c.gridx = 1;
		c.gridy = 50;
		browser.setPreferredSize(new Dimension(600, 400));
		add(browser, c);
		if (password == null || password.isEmpty()) {
			GoogleApiContext.load();
		} else {
			GoogleApiContext.load(password);
		}
		browser.loadHTML(getHTML());
	}

	/**
	 * Constructeur avec UI de demande de mot de passe
	 * 
	 * @throws NoAPIKeyDefineException
	 * 
	 */
	public Map() throws NoAPIKeyDefineException {
		this(null);
	}

	/**
	 * Replace le centre de la carte a la position GPS indiqu�e
	 * 
	 * @param gps
	 */
	public void setCenter(LatLng gps) {
		browser.executeJavaScript("map.setCenter(new google.maps.LatLng(" + gps.lat + "," + gps.lng + "));");
	}

	/**
	 * Choisit le zoom (entre 0 et 21) plafone si la valeur depasse
	 * 
	 * @param zoom
	 */
	public void setZoom(int zoom) {
		int z = (zoom < 22) ? (zoom > -1) ? zoom : 0 : 21;
		browser.executeJavaScript("map.setZoom(" + z + ")");
	}

	/**
	 * Met en place l'outil recherche de localisation par adresse
	 * 
	 */
	public void addSearchTool() {

		st = new SearchTool(this);
		c.gridy = 0;
		c.gridx = 1;
		c.weighty = 0;
		c.weightx = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		this.add(st.getPanel(), c);
	}

	/**
	 * Met en place l'outil de gestion de marker en UI
	 */
	public void addMarkerTool() {
		ma = new MarkerTool(this);
		c.gridy = 0;
		c.gridx = 3;
		c.weightx = 0;
		c.weighty = 1;
		c.gridwidth = 1;
		c.gridheight = 100;
		this.add(ma.getPanel(), c);
	}

	/**
	 * Met en place l'interface de dessin sur la carte
	 */
	public void addDrawerTool() {
		dt = new DrawerTool(this);
		browser.loadHTML(getHTML());
		;
	}

	/**
	 * met en place une interface de dessin specifique
	 */
	public void addDrawerTool(DrawerTool dt) {
		this.dt = dt;
		browser.loadHTML(getHTML());
		;
	}

	/**
	 * �et en place le boutton pour sauvegarder l'image affich�e
	 */
	public void addImageSaverTool() {
		c.gridy = 51;
		c.gridx = 1;
		c.weightx = 1;
		c.weighty = 0;
		c.gridwidth = 1;
		c.gridheight = 1;
		JButton save = new JButton(getSaveButtonTranslatedText());
		save.setMargin(new Insets(8, 0, 8, 0));
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				if (fc.showSaveDialog(Map.this) == JFileChooser.APPROVE_OPTION) {
					try {
						ImageIO.write(Map.this.getDisplayedImage(), "PNG", fc.getSelectedFile());
					} catch (IOException e1) {
						JOptionPane.showMessageDialog(Map.this, getErrorSaveTranslatedText(e1));
					}
				}
			}
		});
		this.add(save, c);
	}

	private String getErrorSaveTranslatedText(IOException e1) {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Une Erreur s'est produite pendant la sauvegarde de l'image.";
		default:
			return "An Error occured during the saving of the picture.";
		}
	}

	private String getSaveButtonTranslatedText() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Sauvegarder la carte affich�e comme une image.";
		default:
			return "Save the current displayed map as a picture.";
		}
	}

	/**
	 * Renvoie le script de chargement de la page
	 * 
	 * @return
	 */
	private String getHTML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<!DOCTYPE html>\n");
		sb.append("<html>\n");
		sb.append(" <head>\n");
		sb.append("  <meta name=\"viewport\" content=\"initial-scale=1.0, user-scalable=no\"/>\n");
		sb.append("  <style type=\"text/css\">\n");
		sb.append("    html { height: 100% }\n");
		sb.append("    body { height: 100%; margin: 0; padding: 0 }\n");
		sb.append("    #map-canvas { height: 100% }\n");
		sb.append("  </style>\n");
		sb.append(
				"  <script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?v=3.exp&libraries=drawing&key="
						+ GoogleApiContext.getKey() + "\"&sensor=true></script>\n");
		sb.append("  <script type=\"text/javascript\">\n");
		sb.append("   var map;\n");
		sb.append("   var polylines =[];\n");
		sb.append("   var polygones =[];\n");
		sb.append("   var markers = [];\n");
		sb.append("   // Adds a marker to the map and push to the array.\n");
		sb.append("   function addMarker(marker) {\n");
		sb.append("    markers.push(marker);\n");
		sb.append("   }\n");
		sb.append("   // Sets the map on all markers in the array.\n");
		sb.append("   function setMapOnAll(map) {\n");
		sb.append("    for (var i = 0; i < markers.length; i++) {\n");
		sb.append("     markers[i].setMap(map);\n");
		sb.append("    }\n");
		sb.append("   }\n");
		sb.append("   // Removes the markers from the map, but keeps them in the array.\n");
		sb.append("   function clearMarkers() {\n");
		sb.append("    setMapOnAll(null);\n");
		sb.append("   }\n");
		sb.append("   // show a specific marker\n");
		sb.append("   function showMarker(index){\n");
		sb.append("    markers[index].setMap(map);\n");
		sb.append("   }\n");
		sb.append("   // Shows any markers currently in the array.\n");
		sb.append("   function showMarkers() {\n");
		sb.append("    setMapOnAll(map);\n");
		sb.append("   }\n");
		sb.append("   // Hide a specific marker\n");
		sb.append("   function hideMarker(index){\n");
		sb.append("    markers[index].setMap(null);\n");
		sb.append("   }\n");
		sb.append("   // Hide all markers\n");
		sb.append("   function hideMarkers(){\n");
		sb.append("    clearMarkers();\n");
		sb.append("   }\n");
		sb.append("   // Deletes all markers in the array by removing references to them.\n");
		sb.append("   function deleteMarkers() {\n");
		sb.append("    clearMarkers();\n");
		sb.append("    markers = [];\n");
		sb.append("   }\n");
		sb.append("   // Deletes a specifics marker \n");
		sb.append("   function deleteMarker(markerIdToDelete) {\n");
		sb.append("    clearMarkers();\n");
		sb.append("    markers.splice(markerIdToDelete,1);\n");
		sb.append("    showMarkers();\n");
		sb.append("   }\n");
		sb.append("   function fitmarkersBounds(){\n");
		sb.append("    var bounds = new google.maps.LatLngBounds();\n");
		sb.append("    for(i=0;i<markers.length;i++) {\n");
		sb.append("     if(markers[i].getMap()!=null)bounds.extend(markers[i].getPosition(markers[i]));\n");
		sb.append("    }\n");
		sb.append("    //center the map to the geometric center of all markers\n");
		sb.append("    map.setCenter(bounds.getCenter());\n");
		sb.append("    map.fitBounds(bounds);\n");
		sb.append("    //remove one zoom level to ensure no marker is on the edge.\n");
		sb.append("    map.setZoom(map.getZoom()-1);\n");
		sb.append("    // set a minimum zoom \n");
		sb.append("    if(map.getZoom()> 15){\n");
		sb.append("     map.setZoom(15)\n");
		sb.append("    }\n");
		sb.append("   }\n");
		sb.append("   function initialize() {\n");
		sb.append("    var mapOptions = {\n");
		sb.append("     center: new google.maps.LatLng(48.209331, 16.381302),\n");
		sb.append("     zoom: 4\n");
		sb.append("    };\n");
		sb.append("    map = new google.maps.Map(document.getElementById(\"map-canvas\"), mapOptions);\n");
		if (dt != null) {
			sb.append(dt);
		}
		sb.append("   }\n");
		sb.append("   google.maps.event.addDomListener(window, 'load', initialize);\n");
		sb.append("  </script>\n");
		sb.append(" </head>\n");
		sb.append(" <body>\n");
		sb.append("  <div id=\"map-canvas\"></div>\n");
		sb.append(" </body>\n");
		sb.append("</html>\n");
		DebugOutput.println(sb.toString());
		return sb.toString();
	}

	/**
	 * Definit un cadre pour les bord de la carte
	 * 
	 * @param ne
	 * @param sw
	 */
	public void fitBounds(LatLng ne, LatLng sw) {
		browser.executeJavaScript("var bounds = new google.maps.LatLngBounds();" + "var ne = new google.maps.LatLng("
				+ ne.lat + ", " + ne.lng + ");" + "var sw = new google.maps.LatLng(" + sw.lat + ", " + sw.lng + ");"
				+ "bounds.extend(ne);" + "bounds.extend(sw);" + "map.fitBounds(bounds);");
	}

	/**
	 * Definit un cadre pour les bord de la carte
	 * 
	 * @param b
	 */
	public void fitBounds(Bounds b) {
		fitBounds(b.northeast, b.southwest);
	}

	/**
	 * Ajoute un marker a la carte et a sa MarkerList (apparaitra dans le
	 * MarkerTool si il est actif)
	 * 
	 * @param m
	 */
	public void addMarker(Marker m) {
		if (ml.add(m)) {
			browser.executeJavaScript("var m = " + m.getJavaScriptDefinition() + ";\n addMarker(m);");
		}
	}

	/**
	 * Ajoute une collection de markers a la carte et a sa MarkerList
	 * (apparaitra dans le MarkerTool si il est actif)
	 * 
	 * @param l
	 */
	public void addMarkers(Collection<Marker> l) {
		for (Marker m : l) {
			addMarker(m);
		}
	}

	/**
	 * Supprime un marker de la carte et a de sa MarkerList (apparaitra dans le
	 * MarkerTool si il est actif)
	 * 
	 * @param m
	 * @return
	 */
	public Marker deleteMarker(Marker m) {
		int i = ml.indexOf(m);
		Marker out = null;
		if (i != -1) {
			out = ml.removeMarkerAt(i);
			ma.update();
			browser.executeJavaScript("deleteMarker(" + i + ");");
		}
		return out;
	}

	/**
	 * Supprime tous les markers de la carte et a de sa MarkerList (apparaitra
	 * dans le MarkerTool si il est actif)
	 * 
	 */
	public void deleteMarkers() {
		ml.clear();
		ma.update();
		browser.executeJavaScript("deleteMarkers();");

	}

	/**
	 * Renvoie la liste de marker associer a la carte(l'utilisation direct de
	 * cet Objet ne mettra pas a jour le MarkerTool)
	 * 
	 * @return
	 */
	public MarkerList getMarkerList() {
		return ml;
	}

	/**
	 * Supprime le Marker d'indexe i et le retourne (null si en dehors des
	 * limite de la liste)
	 * 
	 * @param i
	 * @return
	 */
	public Marker deleteMarker(int i) {
		if (i < 0 || i >= ml.size())
			return null;
		Marker out = ml.removeMarkerAt(i);
		ma.update();
		browser.executeJavaScript("deleteMarker(" + i + ");");
		return out;
	}

	/**
	 * Cache tous les marqueurs de la carte et met a jour le MarkerTool
	 */
	public void hideMarkers() {
		ml.hideMarkers();
		ma.update();
		browser.executeJavaScript("hideMarkers();");
	}

	/**
	 * Cache le marqueur de l'indexe i de la carte et met a jour le MarkerTool
	 * 
	 * @param i
	 */
	public void hideMarker(int i) {
		if (i < 0 || i >= ml.size())
			return;
		ml.hideMarker(i);
		ma.update();
		browser.executeJavaScript("hideMarker(" + i + ");");
	}

	/**
	 * Affiche le marqueur de l'indexe i de la carte et met a jour le MarkerTool
	 * 
	 * @param i
	 */
	public void showMarker(int i) {
		if (i < 0 || i >= ml.size())
			return;
		ml.showMarker(i);
		ma.update();
		browser.executeJavaScript("showMarker(" + i + ");");
	}

	/**
	 * Affiche tous les marqueurs de la carte et met a jour le MarkerTool
	 */
	public void showMarkers() {
		ml.showMarkers();
		ma.update();
		browser.executeJavaScript("showMarkers();");
	}

	/**
	 * Ajuste le zoom et le centre de la carte pour afficher tous les marqueurs
	 * visibles affich�s
	 */
	public void fitBoundsToMarkerList() {
		if (ml.size() == 0)
			return;
		browser.executeJavaScript("fitmarkersBounds();");
	}

	public boolean addPolygone(Polygone polygone) {
		DebugOutput.println(polygone.getDefinition() + "polygones.push(flightPath);\n");
		browser.executeJavaScript(polygone.getDefinition() + "polygones.push(flightPath);\n");
		return plg.add(polygone);
	}

	public boolean addPolygones(Collection<Polygone> c) {
		boolean out = true;
		for (Polygone polyline : c) {
			out &= addPolygone(polyline);
		}
		return out;

	}

	public boolean deletePolygone(int ind) {
		hidePolygone(ind);
		DebugOutput.println("polygones.splice(" + ind + ",1);\n");
		browser.executeJavaScript("polygones.splice(" + ind + ",1);\n");
		return plg.remove(ind);
	}

	public void showPolygone(int ind) {
		plg.get(ind).show();
		String cmd = "polygones[" + ind + "].setMap(map)";
		DebugOutput.println(cmd);
		browser.executeJavaScript(cmd);
	}

	public void showPolygones() {
		for (int i = 0; i < plg.size(); i++) {
			showPolygone(i);
		}
	}

	public void hidePolygone(int ind) {
		plg.get(ind).hide();
		String cmd = "polygones[" + ind + "].setMap(null)";
		DebugOutput.println(cmd);
		browser.executeJavaScript(cmd);
	}

	public void hidePolygones() {
		for (int i = 0; i < plg.size(); i++) {
			hidePolyline(i);
		}
	}

	public PolygoneList getPolygoneList() {
		return plg;
	}

	public void fitBoundsToPolygone(int i) {
		if (plg.size() < i || i < 0)
			throw new IndexOutOfBoundsException();
		StringBuilder sb = new StringBuilder();
		sb.append("    var bounds = new google.maps.LatLngBounds();\n");
		sb.append("    var path = polygones[" + i + "].getPath().getArray();\n");
		sb.append("    if(polygones[" + i + "].getMap()!=null){");
		sb.append("     for(k=0;k<path.length;k++) {\n");
		sb.append("      bounds.extend(path[k]);\n");
		sb.append("     }\n");
		sb.append("    }\n");
		sb.append("    //center the map to the geometric center of all markers\n");
		sb.append("    map.setCenter(bounds.getCenter());\n");
		sb.append("    map.fitBounds(bounds);\n");
		sb.append("    // set a minimum zoom \n");
		sb.append("    if(map.getZoom()> 15){\n");
		sb.append("     map.setZoom(15)\n");
		sb.append("    }\n");
		browser.executeJavaScript(sb.toString());
	}

	public void fitBoundsToPolygones() {
		if (plg.size() == 0)
			return;
		StringBuilder sb = new StringBuilder();
		sb.append("    var bounds = new google.maps.LatLngBounds();\n");
		sb.append("    for(i=0;i<polygones.length;i++) {\n");
		sb.append("     var path = polygones[i].getPath().getArray();\n");
		sb.append("     if(polygones[i].getMap()!=null){");
		sb.append("      for(k=0;k<path.length;k++) {\n");
		sb.append("       bounds.extend(path[k]);\n");
		sb.append("      }\n");
		sb.append("     }\n");
		sb.append("    }\n");
		sb.append("    //center the map to the geometric center of all markers\n");
		sb.append("    map.setCenter(bounds.getCenter());\n");
		sb.append("    map.fitBounds(bounds);\n");
		sb.append("    //remove one zoom level to ensure no marker is on the edge.\n");
		//sb.append("    map.setZoom(map.getZoom()-1);\n");
		sb.append("    // set a minimum zoom \n");
		sb.append("    if(map.getZoom()> 15){\n");
		sb.append("     map.setZoom(15)\n");
		sb.append("    }\n");
		browser.executeJavaScript(sb.toString());
	}

	public boolean addPolyline(Polyline polyline) {
		DebugOutput.println(polyline.getDefinition() + "polylines.push(flightPath);\n");
		if (polyline.size() > 21) {
			return addPolylines(Arrays.asList(polyline.split()));
		} else {
			browser.executeJavaScript(polyline.getDefinition() + "polylines.push(flightPath);\n");
			return pll.add(polyline);
		}
	}

	public boolean addPolylines(Collection<Polyline> c) {
		boolean out = true;
		for (Polyline polyline : c) {
			out &= addPolyline(polyline);
		}
		return out;

	}

	public boolean deletePolyline(int ind) {
		hidePolyline(ind);
		DebugOutput.println("polylines.splice(" + ind + ",1);\n");
		browser.executeJavaScript("polylines.splice(" + ind + ",1);\n");
		return pll.remove(ind);
	}

	public void showPolyline(int ind) {
		pll.get(ind).show();
		String cmd = "polylines[" + ind + "].setMap(map)";
		DebugOutput.println(cmd);
		browser.executeJavaScript(cmd);
	}

	public void showPolylines() {
		for (int i = 0; i < pll.size(); i++) {
			showPolyline(i);
		}
	}

	public void hidePolyline(int ind) {
		pll.get(ind).hide();
		String cmd = "polylines[" + ind + "].setMap(null)";
		DebugOutput.println(cmd);
		browser.executeJavaScript(cmd);
	}

	public void hidePolylines() {
		for (int i = 0; i < pll.size(); i++) {
			hidePolyline(i);
		}
	}

	public PolylineList getPolylineList() {
		return pll;
	}

	public void fitBoundsToPolyline(int i) {
		if (pll.size() < i || i < 0)
			throw new IndexOutOfBoundsException();
		StringBuilder sb = new StringBuilder();
		sb.append("    var bounds = new google.maps.LatLngBounds();\n");
		sb.append("    var path = polylines[" + i + "].getPath().getArray();\n");
		sb.append("    if(polylines[" + i + "].getMap()!=null){");
		sb.append("     for(k=0;k<path.length;k++) {\n");
		sb.append("      bounds.extend(path[k]);\n");
		sb.append("     }\n");
		sb.append("    }\n");
		sb.append("    //center the map to the geometric center of all markers\n");
		sb.append("    map.setCenter(bounds.getCenter());\n");
		sb.append("    map.fitBounds(bounds);\n");
		sb.append("    //remove one zoom level to ensure no marker is on the edge.\n");
		// sb.append(" map.setZoom(map.getZoom()-1);\n");
		sb.append("    // set a minimum zoom \n");
		sb.append("    if(map.getZoom()> 15){\n");
		sb.append("     map.setZoom(15)\n");
		sb.append("    }\n");
		browser.executeJavaScript(sb.toString());
	}

	public void fitBoundsToPolylines() {
		if (pll.size() == 0)
			return;
		StringBuilder sb = new StringBuilder();
		sb.append("    var bounds = new google.maps.LatLngBounds();\n");
		sb.append("    for(i=0;i<polylines.length;i++) {\n");
		sb.append("     var path = polylines[i].getPath().getArray();\n");
		sb.append("     if(polylines[i].getMap()!=null){");
		sb.append("      for(k=0;k<path.length;k++) {\n");
		sb.append("       bounds.extend(path[k]);\n");
		sb.append("      }\n");
		sb.append("     }\n");
		sb.append("    }\n");
		sb.append("    //center the map to the geometric center of all markers\n");
		sb.append("    map.setCenter(bounds.getCenter());\n");
		sb.append("    map.fitBounds(bounds);\n");
		sb.append("    //remove one zoom level to ensure no marker is on the edge.\n");
		//sb.append("    map.setZoom(map.getZoom()-1);\n");
		sb.append("    // set a minimum zoom \n");
		sb.append("    if(map.getZoom()> 15){\n");
		sb.append("     map.setZoom(15)\n");
		sb.append("    }\n");
		browser.executeJavaScript(sb.toString());
	}

	public BufferedImage getDisplayedImage() {
		return browser.getScreen();
	}
}
