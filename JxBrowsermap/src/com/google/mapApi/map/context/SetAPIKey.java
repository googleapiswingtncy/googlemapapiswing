package com.google.mapApi.map.context;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
/**
 * Fenetre de dialog permettant la saisie d'un cl�e d'api et le mot de passe ui y sera associ�
 * 
 *  This project and all these classes can be used without restriction for open
 * source or academic applications as part of projects with pedagogical purpose.
 * It remains the full property of its contributors and any use by enterprises
 * or companies need a written agreement between the contributors and the said
 * company.
 * 
 * @author Arnault Le Pr�vost-Corvellec
 *
 */
public class SetAPIKey extends JDialog {
	private static final long serialVersionUID = 1916783713662701906L;
	private String password;
	private String APIkey;
	private boolean okPressed = false;

	public SetAPIKey() {
		super();
		setModal(true);
		setTitle(getTranslatedTitle());
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = 1;
		c.fill = GridBagConstraints.BOTH;

		c.insets = new Insets(5, 12, 0, 12);
		add(new JLabel(getAPITextTranslatedText()), c);

		final JPasswordField pswd = new JPasswordField();
		final JTextField apiKeyField = new JTextField();
		apiKeyField.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					pswd.requestFocus();
			}
		});
		c.gridy++;
		c.insets = new Insets(2, 10, 5, 10);
		add(apiKeyField, c);

		c.gridy++;
		c.insets = new Insets(10, 12, 0, 12);
		add(new JLabel(getTranslatedText()), c);

		final JPasswordField pswdConf = new JPasswordField();
		pswd.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					pswdConf.requestFocus();
				;
			}
		});
		c.gridy++;
		c.insets = new Insets(2, 10, 5, 10);
		add(pswd, c);

		c.gridy++;
		c.insets = new Insets(5, 12, 0, 12);
		add(new JLabel(getConfirmTranslatedText()), c);

		pswdConf.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					closeDial(pswd, pswdConf,apiKeyField);
			}
		});
		c.gridy++;
		c.insets = new Insets(2, 10, 5, 10);
		add(pswdConf, c);

		JButton ok = new JButton(getButtonTranslatedText());
		ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closeDial(pswd, pswdConf,apiKeyField);
			}
		});
		c.gridy++;
		c.insets = new Insets(5, 40, 10, 40);
		add(ok, c);

		pack();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension position = new Dimension(screenSize.width / 2 - getWidth() / 2,
				screenSize.height / 2 - getHeight() / 2);
		setLocation(position.width, position.height);

	}

	private void closeDial(JPasswordField pswd, JPasswordField pswdConf, JTextField APIkeyField) {
		password = new String(pswd.getPassword());
		Color color = new Color(255, 128, 128);
		if (password.length() < 6) {
			pswdConf.setBackground(Color.white);
			pswd.setBackground(color);
		} else if (password.equals(new String(pswdConf.getPassword()))) {
			APIkey= APIkeyField.getText().trim();
			setVisible(false);
			okPressed = true;
		} else {
			pswd.setBackground(Color.white);
			pswdConf.setBackground(color);
		}
	}

	private String getAPITextTranslatedText() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Entrer la cl� API :";
		default:
			return "Enter the API key :";
		}
	}

	private String getButtonTranslatedText() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Valider";
		default:
			return "OK";
		}
	}

	private String getConfirmTranslatedText() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Confirmer :";
		default:
			return "Confirm :";
		}
	}

	private static String getTranslatedTitle() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Definir la cl� d'API Google";
		default:
			return "Define the Google's API key";
		}
	}

	private static String getTranslatedText() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Definissez le mot de passe pour acceder aux fonctionnalit�s avanc�es :";
		default:
			return "Define the password to access to advanced features :";
		}
	}

	public String getPassword() {
		return password;
	}

	public boolean wasOkPressed() {
		return okPressed;
	}

	public String getAPIkey() {
		return APIkey;
	}

}
