package com.google.mapApi.map.context;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

/**
 * Fenetre de Dialogue demandant le mot de passe a l'utilisateur pour acceder au
 * stockage de la cl� d'api
 * 
 *  This project and all these classes can be used without restriction for open
 * source or academic applications as part of projects with pedagogical purpose.
 * It remains the full property of its contributors and any use by enterprises
 * or companies need a written agreement between the contributors and the said
 * company.
 * 
 * @author Arnault Le Pr�vost-Corvellec
 *
 */
public class GetPassword extends JDialog {
	private static final long serialVersionUID = 1916783713662701906L;
	private String password;
	private boolean okPressed = false;

	public GetPassword() {
		super();
		setTitle(getTranslatedTitle());
		setModal(true);
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = 1;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(10, 10, 5, 10);
		add(new JLabel(getTranslatedText()), c);

		final JPasswordField pswd = new JPasswordField();
		pswd.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER && pswd.getPassword().length != 0)
					closeDial(pswd);
			}
		});
		c.gridy++;
		c.insets = new Insets(5, 10, 5, 10);
		add(pswd, c);

		JButton ok = new JButton(getButtonTranslatedText());
		ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closeDial(pswd);
			}
		});
		c.gridy++;
		c.insets = new Insets(5, 20, 10, 20);
		add(ok, c);

		pack();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension position = new Dimension(screenSize.width / 2 - getWidth() / 2,
				screenSize.height / 2 - getHeight() / 2);
		setLocation(position.width, position.height);

	}

	private void closeDial(JPasswordField pswd) {
		password = new String(pswd.getPassword());
		setVisible(false);
		okPressed = true;
	}

	private String getButtonTranslatedText() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Valider";
		default:
			return "OK";
		}
	}

	private static String getTranslatedTitle() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Saisir le mot de passe";
		default:
			return "Enter the password";
		}
	}

	private static String getTranslatedText() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Saisir le mot de passe pour acceder aux fonctionnalit�s avanc�es";
		default:
			return "Enter the password to access to advanced features";
		}
	}

	public String getPassword() {
		return password;
	}

	public boolean wasOkPressed() {
		return okPressed;
	}
}
