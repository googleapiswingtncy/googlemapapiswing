package com.google.mapApi.map.context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStore.PasswordProtection;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.crypto.SecretKey;
import javax.swing.JOptionPane;

import com.google.mapApi.tool.DebugOutput;
import com.google.maps.GeoApiContext;

/**
 * Class mettant en place un methode pour stocker de maniere securisee une cles
 * d'application google
 * 
 *  This project and all these classes can be used without restriction for open
 * source or academic applications as part of projects with pedagogical purpose.
 * It remains the full property of its contributors and any use by enterprises
 * or companies need a written agreement between the contributors and the said
 * company.
 * 
 * @author Arnault Le Pr�vost-Corvellec
 *
 */
public class GoogleApiContext {
	private static GeoApiContext context;
	private static String key;

	public static void load() throws NoAPIKeyDefineException {
		GetPassword frame = null;
		try {
			File f = new File("googleKey.storage");
			if (!f.exists())
				throw new FileNotFoundException();
			frame = new GetPassword();
			frame.setVisible(true);
			if (!frame.wasOkPressed()) {
				throw new NoAPIKeyDefineException("Wrong Password");
			}
			load(frame.getPassword());
		} catch (FileNotFoundException e) {
			SetAPIKey f = new SetAPIKey();
			f.setVisible(true);
			if (!f.wasOkPressed()) {
				throw new NoAPIKeyDefineException("No API key define");
			}
			init(f.getAPIkey(), f.getPassword());
			load(f.getPassword());
		} 
	}

	/**
	 * charge le keystorage demande le mot de passe (et insiste si il est faux)
	 * et initialise le GeoApiContext
	 * @throws NoAPIKeyDefineException 
	 * 
	 */
	public static void load(String password) throws NoAPIKeyDefineException {
		try {
			File f = new File("googleKey.storage");
			if (!f.exists())
				throw new FileNotFoundException();
			KeyStore storer = KeyStore.getInstance("JCEKS");
			storer.load(new FileInputStream(f), password.toCharArray());
			key = new String(storer.getKey("googleApiKey", password.toCharArray()).getEncoded(), "UTF-8");
			context = new GeoApiContext().setApiKey(key);
			context.setQueryRateLimit(3).setConnectTimeout(1, TimeUnit.SECONDS).setReadTimeout(1, TimeUnit.SECONDS)
					.setWriteTimeout(1, TimeUnit.SECONDS);
		} catch (FileNotFoundException e) {
			SetAPIKey f = new SetAPIKey();
			f.setVisible(true);
			if (!f.wasOkPressed()) {
				throw new NoAPIKeyDefineException("No API key define");			}
			init(f.getAPIkey(), f.getPassword());
			load(f.getPassword());
		} catch (IOException e) {
			if (e.getMessage().equals("Keystore was tampered with, or password was incorrect")) {
				switch (Locale.getDefault().getLanguage()) {
				case "fr":
					JOptionPane.showMessageDialog(null, "Mot de passse incorect.");
					break;
				default:
					JOptionPane.showMessageDialog(null, "Wrong password.");
					break;
				}
				load();
			} else {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * initialise le keystorer en definissant le mot de passe d'acc�es
	 * 
	 * @param key
	 *            la cl�e d'api a stocker
	 * @param psw
	 */
	public static void init(String key, String password) {
		try {
			KeyStore storer = KeyStore.getInstance("JCEKS");
			storer.load(null, password.toCharArray());
			SecretKey sk = new SecretKey() {
				private static final long serialVersionUID = 4043704173434868494L;

				public String getFormat() {
					return null;
				}

				public byte[] getEncoded() {
					return key.getBytes();
				}

				public String getAlgorithm() {
					return "GoogleApiKey";
				}
			};
			KeyStore.SecretKeyEntry keyStoreEntry = new KeyStore.SecretKeyEntry(sk);
			PasswordProtection keyPassword = new PasswordProtection(password.toCharArray());
			storer.setEntry("googleApiKey", keyStoreEntry, keyPassword);
			File f = new File("googleKey.storage");
			DebugOutput.println(f.getAbsolutePath());
			if (!f.exists()) {
				f.createNewFile();
			}
			storer.store(new FileOutputStream(f), password.toCharArray());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Renvoie le contexte necessaire a l'utilisation de l api
	 * 
	 * @return
	 */
	public static GeoApiContext getContext() {
		return context;
	}

	/**
	 * renvoie la cl�e d'api si le conteneur a �t� deverouill�
	 * 
	 * @return
	 */
	public static String getKey() {
		return key;
	}

}
