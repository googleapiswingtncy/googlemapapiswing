package com.google.mapApi.map.context;

/**
 * Une exception personnalis�e pour signaler que la cl� d'api n'as pas ete
 * rentrer ou que le mot de passe entr� est incorrect
 * 
 * This project and all these classes can be used without restriction for open
 * source or academic applications as part of projects with pedagogical purpose.
 * It remains the full property of its contributors and any use by enterprises
 * or companies need a written agreement between the contributors and the said
 * company.
 * 
 * @author Arnault Le Pr�vost-Corvellec
 *
 */
public class NoAPIKeyDefineException extends Exception {

	public NoAPIKeyDefineException(String string) {
		super(string);
	}

	private static final long serialVersionUID = -8713397494488232130L;

}
