package com.google.mapApi.map.addon.searchTool;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.google.mapApi.map.Map;
import com.google.mapApi.map.addon.MapControler;
import com.google.mapApi.map.context.GoogleApiContext;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

/**
 * Classe implementant le champ de recherche pour deplacer la carte en donnant
 * le nom d'une ville
 * 
 * This project and all these classes can be used without restriction for open
 * source or academic applications as part of projects with pedagogical purpose.
 * It remains the full property of its contributors and any use by enterprises
 * or companies need a written agreement between the contributors and the said
 * company.
 * 
 * @author Arnault Le Pr�vost-Corvellec
 *
 */

public class SearchTool extends MapControler {
	private JPanel panel;
	private JTextField jtf;
	private JButton searchButton;

	/**
	 * Constructeur
	 * 
	 * @param map
	 */
	public SearchTool(Map map) {
		super(map);
		panel = new JPanel(new BorderLayout());
		jtf = new JTextField();
		jtf.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					GeoApiContext gc = GoogleApiContext.getContext();
					try {
						GeocodingResult[] results = GeocodingApi.newRequest(gc).address(jtf.getText().trim()).await();
						LatLng loc = results[0].geometry.location;
						LatLng ne = results[0].geometry.viewport.northeast;
						LatLng sw = results[0].geometry.viewport.southwest;
						
						SearchTool.this.map.setCenter(loc);
						SearchTool.this.map.fitBounds(ne,sw);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		panel.add(jtf, BorderLayout.CENTER);
		searchButton = new JButton();
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			searchButton.setText("Rechercher");
			break;
		default:
			searchButton.setText("Search");
			break;
		}
		searchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GeoApiContext gc = GoogleApiContext.getContext();
				try {
					if (jtf.getText().trim().length() == 0)
						return;
					GeocodingResult[] results = GeocodingApi.newRequest(gc).address(jtf.getText().trim()).await();
					LatLng loc = results[0].geometry.location;
					LatLng ne = results[0].geometry.viewport.northeast;
					LatLng sw = results[0].geometry.viewport.southwest;
					SearchTool.this.map.setCenter(loc);
					SearchTool.this.map.fitBounds(ne,sw);

				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		panel.add(searchButton, BorderLayout.EAST);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.tncyhelper.map.MapControler#getPanel()
	 */
	public JPanel getPanel() {
		return panel;
	}

}
