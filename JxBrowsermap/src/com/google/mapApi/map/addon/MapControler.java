package com.google.mapApi.map.addon;

import java.util.Observable;

import javax.swing.JPanel;

import com.google.mapApi.map.Map;

/**
 * La classe abstraite des controller de la carte
 * 
 * This project and all these classes can be used without restriction for open
 * source or academic applications as part of projects with pedagogical purpose.
 * It remains the full property of its contributors and any use by enterprises
 * or companies need a written agreement between the contributors and the said
 * company.
 * 
 * @author Arnault Le Pr�vost-Corvellec
 *
 */
public abstract class MapControler extends Observable {
	protected Map map;
	/** Constructeur
	 * @param map
	 */
	public MapControler(Map map) {
		this.setMap(map);
	}
	
	/** renvoie l'ui de controle
	 * @return
	 */
	public abstract JPanel getPanel() ;

	/** Renvoie la carte sur laquelle l'outil travail
	 * @return
	 */
	public Map getMap() {
		return map;
	}

	/** Definit la carte sur laquelle l'outil va travailler
	 * @param map
	 */
	private void setMap(Map map) {
		this.map = map;
	}
}
