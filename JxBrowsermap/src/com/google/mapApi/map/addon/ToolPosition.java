package com.google.mapApi.map.addon;

public class ToolPosition {
	/**
	 * Indique que la commande doit �tre plac�e en haut au centre de la carte.
	 */
	public final static ToolPosition TOP_CENTER = new ToolPosition("TOP_CENTER");
	/**
	 * Indique que la commande doit �tre plac�e en haut � gauche de la carte, et
	 * que tous les sous-�l�ments de la commande sont r�partis vers le centre
	 * sup�rieur.
	 */
	public final static ToolPosition TOP_LEFT = new ToolPosition("TOP_LEFT");
	/**
	 * Indique que la commande doit �tre plac�e en haut � droite de la carte, et
	 * que tous les sous-�l�ments de la commande sont r�partis vers le centre
	 * sup�rieur.
	 */
	public final static ToolPosition TOP_RIGHT = new ToolPosition("TOP_RIGHT");
	/**
	 * Indique que la commande doit �tre plac�e en haut � gauche de la carte,
	 * mais en dessous de tout �l�ment TOP_LEFT.
	 */
	public final static ToolPosition LEFT_TOP = new ToolPosition("LEFT_TOP");
	/**
	 * Indique que la commande doit �tre plac�e en haut � droite de la carte,
	 * mais en dessous de tout �l�ment TOP_RIGHT.
	 */
	public final static ToolPosition RIGHT_TOP = new ToolPosition("RIGHT_TOP");
	/**
	 * Indique que la commande doit �tre plac�e sur le c�t� gauche de la carte,
	 * centr�e entre les positions TOP_LEFT et BOTTOM_LEFT.
	 */
	public final static ToolPosition LEFT_CENTER = new ToolPosition("LEFT_CENTER");
	/**
	 * Indique que la commande doit �tre plac�e sur le c�t� droit de la carte,
	 * centr�e entre les positions TOP_RIGHT et BOTTOM_RIGHT.
	 */
	public final static ToolPosition RIGHT_CENTER = new ToolPosition("RIGHT_CENTER");
	/**
	 * Indique que la commande doit �tre plac�e en bas � gauche de la carte,
	 * mais au-dessus de tout �l�ment BOTTOM_LEFT.
	 */
	public final static ToolPosition LEFT_BOTTOM = new ToolPosition("LEFT_BOTTOM");
	/**
	 * Indique que la commande doit �tre plac�e en bas � droite de la carte,
	 * mais au-dessus de tout �l�ment BOTTOM_RIGHT.
	 */
	public final static ToolPosition RIGHT_BOTTOM = new ToolPosition("RIGHT_BOTTOM");
	/**
	 * Indique que la commande doit �tre plac�e en bas au centre de la carte.
	 */
	public final static ToolPosition BOTTOM_CENTER = new ToolPosition("BOTTOM_CENTER");
	/**
	 * Indique que la commande doit �tre plac�e en bas � gauche de la carte, et
	 * que tous les sous-�l�ments de la commande sont r�partis vers le centre
	 * inf�rieur.
	 */
	public final static ToolPosition BOTTOM_LEFT = new ToolPosition("BOTTOM_LEFT");
	/**
	 * Indique que la commande doit �tre plac�e en bas � droite de la carte, et
	 * que tous les sous-�l�ments de la commande sont r�partis vers le centre
	 * inf�rieur.
	 */
	public final static ToolPosition BOTTOM_RIGHT = new ToolPosition("BOTTOM_RIGHT");
	
	private String position;

	private ToolPosition(String position) {
		this.position=position;
	}

	@Override
	public String toString() {
		return position;
	}
}
