package com.google.mapApi.map.addon.drawer;

import javax.swing.JPanel;

import com.google.mapApi.map.Map;
import com.google.mapApi.map.addon.MapControler;
import com.google.mapApi.map.addon.ToolPosition;
import com.google.mapApi.tool.DebugOutput;

public class DrawerTool extends MapControler {
	private StringBuilder sb = new StringBuilder();
	private ToolPosition position;
	private boolean allowCircle;
	private boolean allowPolygon;
	private boolean allowPolyline;
	private boolean allowRectangle;
	private CircleOptions circleOption;

	public DrawerTool(Map map) {
		this(map, ToolPosition.TOP_CENTER, true, new CircleOptions(), true, true, true);

	}

	public DrawerTool(Map map, ToolPosition position, boolean allowCircle, CircleOptions circleOption,
			boolean allowPolygon, boolean allowpolyline, boolean allowRectangle) {
		super(map);
		setPosition(position);
		setAllowCircle(allowCircle);
		setCircleOption(circleOption);
		setAllowPolygon(allowPolygon);
		setAllowPolyline(allowpolyline);
		setAllowRectangle(allowRectangle);
		build();
	}

	private void build() {
		if (!(isAllowCircle() || isAllowPolygon() || isAllowPolyline() || isAllowRectangle()))
			return;
		sb = new StringBuilder();
		sb.append("    var drawingManager = new google.maps.drawing.DrawingManager({\n");
		sb.append("     drawingMode: google.maps.drawing.OverlayType.MARKER,\n");
		sb.append("     drawingControl: true,\n");
		sb.append("     drawingControlOptions: {\n");
		sb.append("      position: google.maps.ControlPosition." + position + ",\n");
		sb.append("      drawingModes: [\n");
		// sb.append(" google.maps.drawing.OverlayType.MARKER,\n"); //TODO si on
		// peut renvoier un truc pour les recupere avec le marueur tool on
		// raoutera
		if (isAllowCircle())
			sb.append("       google.maps.drawing.OverlayType.CIRCLE,\n");
		if (isAllowPolygon())
			sb.append("       google.maps.drawing.OverlayType.POLYGON,\n");
		if (isAllowPolyline())
			sb.append("       google.maps.drawing.OverlayType.POLYLINE,\n");
		if (isAllowRectangle())
			sb.append("       google.maps.drawing.OverlayType.RECTANGLE\n");
		sb.append("      ]\n");
		sb.append("     },\n");
		// sb.append(" markerOptions: {icon: 'images/beachflag.png'},\n");
		if (isAllowCircle())
			sb.append(circleOption);
		sb.append("    });\n");
		sb.append("    drawingManager.setMap(map);\n");
	}

	@Override
	public JPanel getPanel() {
		return null;
	}

	public ToolPosition getPosition() {
		return position;
	}

	private void setPosition(ToolPosition position) {
		this.position = position;
	}

	public boolean isAllowCircle() {
		return allowCircle;
	}

	private void setAllowCircle(boolean allowCircle) {
		this.allowCircle = allowCircle;
	}

	public boolean isAllowPolygon() {
		return allowPolygon;
	}

	private void setAllowPolygon(boolean allowPolygon) {
		this.allowPolygon = allowPolygon;
	}

	public boolean isAllowPolyline() {
		return allowPolyline;
	}

	private void setAllowPolyline(boolean allowPolyline) {
		this.allowPolyline = allowPolyline;
	}

	public boolean isAllowRectangle() {
		return allowRectangle;
	}

	private void setAllowRectangle(boolean allowRectangle) {
		this.allowRectangle = allowRectangle;
	}

	public CircleOptions getCircleOption() {
		return circleOption;
	}

	private void setCircleOption(CircleOptions circleOption) {
		this.circleOption = circleOption;
	}

	@Override
	public String toString() {
		DebugOutput.println(sb);
		return sb.toString();
	}
}
