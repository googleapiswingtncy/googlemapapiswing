package com.google.mapApi.map.addon.drawer;

import java.awt.Color;

public class CircleOptions {
	private StringBuilder sb = new StringBuilder();
	private Color color;
	private double fillOpacity;
	private boolean clickable;
	private boolean editable;
	private int zIndex;
	private int strokeWeight;

	public CircleOptions() {
		this(Color.red);
	}

	public CircleOptions(Color color) {
		this(color, 1, true, true, 1, 5);
	}

	public CircleOptions(Color color, double fillOpacity, boolean clickable, boolean editable, int zIndex,
			int strokeWeight) {
		setColor(color);
		setFillOpacity(fillOpacity);
		setClickable(clickable);
		setEditable(editable);
		setzIndex(zIndex);
		setStrokeWeight(strokeWeight);
		build();
	}

	private void build() {
		sb.append("     circleOptions: {\n");
		sb.append("      fillColor: '#" + Integer.toHexString(color.getRGB()).substring(2) + "',\n");
		sb.append("      fillOpacity: " + getFillOpacity() + ",\n");
		sb.append("      strokeWeight: " + getStrokeWeight() + ",\n");
		sb.append("      clickable: " + isClickable() + ",\n");
		sb.append("      editable: " + isEditable() + ",\n");
		sb.append("      zIndex: " + getzIndex() + "\n");
		sb.append("     }\n");
	}

	public Color getColor() {
		return color;
	}

	private void setColor(Color color) {
		this.color = color;
	}

	public boolean isClickable() {
		return clickable;
	}

	private void setClickable(boolean clickable) {
		this.clickable = clickable;
	}

	public double getFillOpacity() {
		return fillOpacity;
	}

	private void setFillOpacity(double fillOpacity) {
		this.fillOpacity = fillOpacity;
	}

	public boolean isEditable() {
		return editable;
	}

	private void setEditable(boolean editable) {
		this.editable = editable;
	}

	public int getzIndex() {
		return zIndex;
	}

	private void setzIndex(int zIndex) {
		this.zIndex = zIndex;
	}

	public int getStrokeWeight() {
		return strokeWeight;
	}

	private void setStrokeWeight(int strokeWeight) {
		this.strokeWeight = strokeWeight;
	}

	@Override
	public String toString() {
		return sb.toString();
	}
}
