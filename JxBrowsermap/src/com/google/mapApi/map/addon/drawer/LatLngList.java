package com.google.mapApi.map.addon.drawer;

import java.util.ArrayList;

import com.google.maps.model.LatLng;

public abstract class LatLngList extends ArrayList<LatLng> {

	private static final long serialVersionUID = 6422890675309479884L;

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		int i = 0;
		for (LatLng gps : this) {

			sb.append("{lat : " + gps.lat + ", lng : " + gps.lng + "},");
			if (i < 10) {
				sb.append("\n");
				i -= 10;
			}
			i++;
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean contains(Object arg0) {
		if (!(arg0 instanceof LatLng))
			return false;
		LatLng c = (LatLng) arg0;
		for (LatLng l : this) {
			if ((l.lat == c.lat) && (l.lng == c.lng))
				return true;
		}
		return false;
	}
}
