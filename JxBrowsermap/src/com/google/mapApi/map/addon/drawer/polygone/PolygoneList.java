package com.google.mapApi.map.addon.drawer.polygone;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Observable;

/**
 * Structure Observable de Marker
 * 
 * 
 * This project and all these classes can be used without restriction for open
 * source or academic applications as part of projects with pedagogical purpose.
 * It remains the full property of its contributors and any use by enterprises
 * or companies need a written agreement between the contributors and the said
 * company.
 * 
 * @author Arnault Le Pr�vost-Corvellec
 *
 */
public class PolygoneList extends Observable implements Collection<Polygone> {
	private ArrayList<Polygone> ml = new ArrayList<Polygone>();

	/**
	 * Ajoute un element a la liste et notifit les observeurs
	 * 
	 * @param m
	 * @return
	 */
	public boolean add(Polygone m) {
		if (ml.contains(m))
			return false;
		else {
			boolean b = ml.add(m);
			notifyObservers();
			return b;
		}
	}

	/**
	 * Ajoute un element a la liste et sans notifier les observeurs
	 * 
	 * @param m
	 * @return
	 */
	private boolean privateAdd(Polygone m) {
		if (ml.contains(m))
			return false;
		else {
			return ml.add(m);
		}
	}

	/**
	 * Ajoute une collection d'elements a la liste et notifit les observeurs
	 * 
	 * @param c
	 * @return
	 */
	@Override
	public boolean addAll(Collection<? extends Polygone> c) {
		for (Polygone marker : c) {
			privateAdd(marker);
		}
		notifyObservers();
		ml.trimToSize();
		return true;
	}

	/**
	 * vide la liste et notifit les observeurs
	 */
	@Override
	public void clear() {
		ml.clear();
		notifyObservers();
	}

	/**
	 * Renvoie true si la liste contient l'element o, false sinon
	 * 
	 * @param o
	 * @return
	 */
	@Override
	public boolean contains(Object o) {
		return ml.contains(o);
	}

	/**
	 * Renvoie true si la liste contient tous les elements de c, false sinon
	 * 
	 * @param c
	 * @return
	 */
	@Override
	public boolean containsAll(Collection<?> c) {
		return ml.contains(c);
	}

	/**
	 * renvoie true si la liste est vide, false sinon
	 * 
	 * @return
	 */
	@Override
	public boolean isEmpty() {
		return ml.size() == 0;
	}

	/**
	 * renvoie l'iterateur de la liste
	 * 
	 * @return
	 */
	@Override
	public Iterator<Polygone> iterator() {
		return ml.iterator();
	}

	/**
	 * Essaie de supprime l'element o de la liste puis notifit les observeurs,
	 * renvoie true si l'operation a r�ussit false sinon
	 * 
	 * @param o
	 * @return
	 */
	@Override
	public boolean remove(Object o) {
		boolean b = ml.remove(o);
		notifyObservers();
		return b;
	}

	/**
	 * Essaie de supprimer tous les elements de c de la liste puis notifie les
	 * observeurs, renvoie true si l'operation a r�ussit false sinon
	 * 
	 * @param c
	 * @return
	 */
	@Override
	public boolean removeAll(Collection<?> c) {
		boolean b = ml.removeAll(c);
		notifyObservers();
		return b;
	}

	/**
	 * Supprime de la liste tous les �lements qui ne sont pas pr�sent dans c
	 * puis notifit les observeurs, renvoie true si l'operation a r�ussit false
	 * sinon
	 * 
	 * @param c
	 * @return
	 */
	@Override
	public boolean retainAll(Collection<?> c) {
		boolean b = ml.retainAll(c);
		ml.trimToSize();
		notifyObservers();
		return b;
	}

	/**
	 * Retourne le nombre d'element de la liste
	 * 
	 * @return
	 */

	@Override
	public int size() {
		return ml.size();
	}

	/**
	 * Renvoie un Object[] contenant tous les element present dans la liste
	 * 
	 * @return
	 */
	@Override
	public Object[] toArray() {
		return ml.toArray();
	}

	/**
	 * Remplit et renvoie un Marker[] contenant les elements de la liste
	 * 
	 * @param a
	 * @return
	 */
	@Override
	public <Polyline> Polyline[] toArray(Polyline[] a) {
		return ml.toArray(a);
	}

	/**
	 * Renvoie le Marker present a l'index index
	 * 
	 * @param index
	 * @return
	 */
	public Polygone get(int index) {
		return ml.get(index);
	}

	/**
	 * Renvoie l'index du Marker m si il est present , -1 sinon
	 * 
	 * @param m
	 * @return
	 */
	public int indexOf(Polygone m) {
		for (int i = 0; i < ml.size(); i++) {
			if (m.equals(ml.get(i)))
				return i;
		}
		return -1;
	}

	/**
	 * Supprime le marker present a l'index i et decalle les element suivant
	 * vers la gauche puis notifit les observeur, renvoie une
	 * OutOfBoundException si i n'est pas un index valide de la liste
	 * 
	 * @param i
	 * @return
	 * @throws IndexOutOfBoundsException
	 */
	public Polygone removeMarkerAt(int i) {
		Polygone m = ml.remove(i);
		notifyObservers();
		return m;
	}

	/**
	 * Renvoie une copie de la liste
	 * 
	 * @return
	 * @throws CloneNotSupportedException
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		PolygoneList out = new PolygoneList();
		for (Polygone m : this) {
			out.add((Polygone) m.clone());
		}
		return out;
	}

	/**
	 * Change la propri�t� d'affichage des Markers de la liste � false puis
	 * notifit les observeurs
	 */
	public void hideMarkers() {
		for (Polygone m : this) {
			m.hide();
		}
		notifyObservers();
	}

	/**
	 * Change la propri�t� d'affichage du Marker a l'indexe i de la liste �
	 * false puis notifit les observeurs
	 * 
	 * @param i
	 */

	public void hideMarker(int i) {

		this.get(i).hide();
		notifyObservers();
	}

	/**
	 * Change la propri�t� d'affichage du Marker a l'indexe i de la liste � true
	 * puis notifit les observeurs
	 * 
	 * @param i
	 */
	public void showMarker(int i) {
		this.get(i).show();
		notifyObservers();
	}

	/**
	 * Change la propri�t� d'affichage des Markers de la liste � true puis
	 * notifit les observeurs
	 */
	public void showMarkers() {
		for (Polygone m : this) {
			m.show();
		}
		notifyObservers();
	}

}
