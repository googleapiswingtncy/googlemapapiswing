package com.google.mapApi.map.addon.drawer.marker;

import java.util.Observable;

import com.google.mapApi.map.addon.drawer.marker.icon.MarkerIcon;
import com.google.maps.model.Bounds;
import com.google.maps.model.LatLng;

/**
 * Structure de donn�es contenant les informations necessaire la cr�ation de
 * Marker pour la carte
 * 
 * This project and all these classes can be used without restriction for open
 * source or academic applications as part of projects with pedagogical purpose.
 * It remains the full property of its contributors and any use by enterprises
 * or companies need a written agreement between the contributors and the said
 * company.
 * 
 * @author Arnault Le Pr�vost-Corvellec
 *
 */
public class Marker extends Observable {
	private LatLng position;
	private MarkerIcon icon;
	private String letter = null;
	private String title;
	private Bounds bounds;

	private boolean displayed;

	/**
	 * Constructeur d'un marqueur sans etiquette, equivalent a Marker(coord,
	 * null, NEBound, SWBound)
	 * 
	 * @param coord
	 *            coordonn�es du marqueur
	 * @param NEBound
	 *            Coordonn�es Nord Est de la limite de l'emplacement
	 * @param SWBound
	 *            Coordonn�es Sud Ouest de la limite de l'emplacement
	 */
	public Marker(LatLng coord, LatLng NEBound, LatLng SWBound) {
		this(coord, null, NEBound, SWBound);
	}

	/**
	 * Constructeur d'un marqueur avec une etiquette
	 * 
	 * @param coord
	 *            coordonn�es du marqueur
	 * @param letter
	 *            Etiquette du Marker(seul le premier charact�re est pris en
	 *            consideration
	 * @param NEBound
	 *            Coordonn�es Nord Est de la limite de l'emplacement
	 * @param SWBound
	 *            Coordonn�es Sud Ouest de la limite de l'emplacement
	 */
	public Marker(LatLng coord, String letter, LatLng NEBound, LatLng SWBound) {
		this(coord, letter, new Bounds());
		bounds.northeast = NEBound;
		bounds.southwest = SWBound;
	}

	/**
	 * Constructeur d'un marqueur avec une etiquette
	 * 
	 * @param coord
	 *            Coordonn�es du marqueur
	 * @param letter
	 *            Etiquette du Marker(seul le premier charact�re est pris en
	 *            consideration
	 * @param bounds
	 *            Limites de l'emplacement
	 */
	public Marker(LatLng coord, String letter, Bounds bounds) {
		this(coord, letter, bounds, null);
	}

	public Marker(LatLng coord, String letter, Bounds bounds, MarkerIcon icon) {
		this(coord, letter, bounds, icon, null);
	}

	public Marker(LatLng coord, String letter, Bounds bounds, MarkerIcon icon, String title) {
		position = coord;
		this.icon = icon;
		if (letter != null && !letter.trim().isEmpty()) {
			setLetter(letter);
		}
		this.setBounds(bounds);
		setDisplayed(true);
	}

	private void setBounds(Bounds bounds) {
		this.bounds = bounds;
	}

	/**
	 * Renvoie la lettre assigner au marker
	 * 
	 * @return
	 */
	public String getLetter() {
		return letter;
	}

	private void setLetter(String letter) {
		this.letter = letter;
	}

	/**
	 * Renvoie la definition JavaScript de l'objet
	 * 
	 * @return
	 */
	public String getJavaScriptDefinition() {
		String s = "new google.maps.Marker({position: {lat:" + position.lat + ", lng:" + position.lng + "}";

		if (isDisplayed())
			s += ", map: map";
		else
			s += ", map: null";

		if (letter != null)
			s += ",label : \"" + letter + "\"";
		if (icon != null)
			s += ",icon : \"" + icon.toString() + "\"";
		if (title != null && !title.isEmpty())
			s += ",title : \"" + title + "\"";
		return s + "})";
	}

	/**
	 * Renvoie les coordonn�es GPS associ�s au marker sous la form
	 * latitude,longitude
	 * 
	 * @return
	 */
	public String getStringFormattedPostion() {
		return position.lat + "," + position.lng;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Marker)
			return position.lat == ((Marker) obj).getLat() && getLng() == ((Marker) obj).getLng()
					&& ((letter == null && ((Marker) obj).letter == null) || (letter.equals(((Marker) obj).letter)));
		else
			return false;
	}

	private double getLng() {
		return position.lng;
	}

	private double getLat() {
		return position.lat;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		Bounds b = new Bounds();
		b.northeast = new LatLng(bounds.northeast.lat, bounds.northeast.lng);
		b.southwest = new LatLng(bounds.southwest.lat, bounds.southwest.lng);
		return new Marker(new LatLng(getLat(), getLng()), new String(letter), b, (MarkerIcon) icon.clone());
	}

	/**
	 * Renvoie les limites de l'adresse marqu�e
	 * 
	 * @return
	 */
	public Bounds getBounds() {
		return bounds;
	}

	/**
	 * Renvoie true si le marqueur est affich�, false sinon
	 * 
	 * @return
	 */
	public boolean isDisplayed() {
		return displayed;
	}

	protected void setDisplayed(boolean displayed) {
		this.displayed = displayed;
		notifyObservers();
	}
	// TODO Create setter with notify and add fields to match to Google's API
}
