package com.google.mapApi.map.addon.drawer.polyline;

import java.awt.Color;
import java.util.Collection;

import com.google.mapApi.map.addon.drawer.LatLngList;
import com.google.maps.model.LatLng;

public class Polyline extends LatLngList {

	private int id;
	private Color strokeColor;
	private double strokeOpacity;
	private int strokeWeight;
	private boolean display = true;

	private static int nextId = 0;

	private static final long serialVersionUID = -9051109851848133820L;

	public Polyline(Color strokeColor, double strokeOpacity, int strokeWeight, Collection<LatLng> c) {
		super();
		if (c != null && !c.isEmpty())
			addAll(c);
		setId(nextId);
		nextId++;
		setStrokeColor(strokeColor);
		setStrokeOpacity(strokeOpacity);
		setStrokeWeight(strokeWeight);
	}

	public Polyline(Collection<LatLng> c) {
		this(Color.RED, 0.35, 1, c);
	}

	public Polyline() {
		this(null);
	}

	public String getDefinition() {
		return getMonoBlockDefinition();
	}
	public String getMonoBlockDefinition() {
		StringBuilder sb = new StringBuilder();
		sb.append("var flightPath = new google.maps.Polyline({\n");
		sb.append(" path : " + super.toString() + ",\n");
		sb.append(" strokeColor : '#" + Integer.toHexString(strokeColor.getRGB()).substring(2) + "',\n");
		sb.append(" strokeOpacity : " + strokeOpacity + ",\n");
		sb.append(" strokeWeight : " + strokeWeight + "\n");
		sb.append("});\n");
		sb.append("flightPath.setMap(map);\n");
		return sb.toString();
	}
	public String getSplitDefintion() {
		Polyline[] pol = split();
		StringBuilder sb = new StringBuilder();
		for(Polyline p : pol){
			sb.append(p.getMonoBlockDefinition());
		}
		return sb.toString();
		
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("points : " + super.toString() + ",");
		sb.append("Color : '#" + Integer.toHexString(strokeColor.getRGB()).substring(2) + "',");
		sb.append("Opacity : " + strokeOpacity + ",");
		sb.append("Weight : " + strokeWeight + ", id : " + id);
		return sb.toString();
	}

	/**
	 * @return the Id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	private void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the strokeColor
	 */
	public Color getStrokeColor() {
		return strokeColor;
	}

	/**
	 * @param strokeColor
	 *            the strokeColor to set
	 */
	public void setStrokeColor(Color strokeColor) {
		this.strokeColor = strokeColor;
	}

	/**
	 * @return the strokeOpacity
	 */
	public double getStrokeOpacity() {
		return strokeOpacity;
	}

	/**
	 * @param strokeOpacity
	 *            the strokeOpacity to set
	 */
	public void setStrokeOpacity(double strokeOpacity) {
		this.strokeOpacity = strokeOpacity;
	}

	/**
	 * @return the strokeWeight
	 */
	public int getStrokeWeight() {
		return strokeWeight;
	}

	/**
	 * @param strokeWeight
	 *            the strokeWeight to set
	 */
	public void setStrokeWeight(int strokeWeight) {
		this.strokeWeight = strokeWeight;
	}

	public void show() {
		this.setDisplay(true);
	}

	public void hide() {
		this.setDisplay(false);
	}

	public boolean isDisplay() {
		return display;
	}

	private void setDisplay(boolean display) {
		this.display = display;
	}

	private static int nbElementParPolylines = 20;

	public Polyline[] split() {
		Polyline[] out = new Polyline[(size() / nbElementParPolylines) + 1];
		for (int i = 0; i < out.length-1; i++) {
			out[i]= new Polyline(strokeColor, strokeOpacity, strokeWeight,null);
			for(int k =0;k<nbElementParPolylines+1;k++){
				out[i].add(this.get(i*nbElementParPolylines+k));
			}			
		}
		int i=out.length-1;
		out[i]= new Polyline(strokeColor,strokeOpacity, strokeWeight,null);
		for(int k =0;i*nbElementParPolylines+k<size();k++){
			out[i].add(this.get(i*nbElementParPolylines+k));
		}
		return out;

	}
}
