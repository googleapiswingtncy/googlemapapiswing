package com.google.mapApi.map.addon.drawer.marker;

import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import javax.swing.table.AbstractTableModel;

/**
 * Implmentation d'un AbstractTable modele pour l'observation d'une MarkerList
 * 
 * 
 * This project and all these classes can be used without restriction for open
 * source or academic applications as part of projects with pedagogical purpose.
 * It remains the full property of its contributors and any use by enterprises
 * or companies need a written agreement between the contributors and the said
 * company.
 * 
 * @author Arnault Le Pr�vost-Corvellec
 *
 */
public class MarkerListAbstractTable extends AbstractTableModel implements Observer {

	private static final long serialVersionUID = -1972684611585733388L;
	private MarkerList ml;
	/**
	 * Constructeur
	 * @param ml
	 */
	public MarkerListAbstractTable(MarkerList ml) {
		this.ml = ml;
		this.ml.addObserver(this);
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public int getRowCount() {
		return ml.size();
	}

	@Override
	public String getColumnName(int col) {
		switch (col) {
		case 0:
			return getCharColumnName();
		case 1:
			return getPositionColumnName();
		case 2:
			return getDisplayColumnName();
		default:
			return null;
		}
	}
	
	private String getDisplayColumnName() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "<html>Affich&eacute</html>";
		default:
			return "Display";
		}
	}

	private String getPositionColumnName() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Localisation";
		default:
			return "Location";
		}
	}

	private String getCharColumnName() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Etiquette";
		default:
			return "Label";
		}
	}

	@Override
	public Object getValueAt(int row, int col) {
		switch (col) {
		case 0:
			return ml.get(row).getLetter();
		case 1:
			return ml.get(row).getStringFormattedPostion();
		case 2:
			return (ml.get(row).isDisplayed()) ? getYes() : getNo();
		default:
			return null;
		}
	}

	private String getNo() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "non";
		default:
			return "no";
		}
	}

	private String getYes() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "oui";
		default:
			return "yes";
		}
	}
	
	protected void update() {
		fireTableDataChanged();
	}

	@Override
	public void update(Observable o, Object arg) {
		fireTableDataChanged();
	}

}
