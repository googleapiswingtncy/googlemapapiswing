package com.google.mapApi.map.addon.drawer.marker.icon;

import java.net.MalformedURLException;
import java.net.URL;

public class MarkerIconFactory {
	/**
	 * Color available : gray, green, orange, purple, red, white, yellow, black,
	 * blue, brown
	 * 
	 * @param color
	 * @return
	 */
	public static MarkerIcon getMiniIcon(String color) {
		try {
			return new MarkerIcon(new URL("http://labs.google.com/ridefinder/images/mm_20_"+color+".png"));
		} catch (MalformedURLException e) {
			return null;
		}
	}
	//TODO faut lister tout les icon dispo la http://stackoverflow.com/questions/8248077/google-maps-v3-standard-icon-shadow-names-equiv-of-g-default-icon-in-v2
}
