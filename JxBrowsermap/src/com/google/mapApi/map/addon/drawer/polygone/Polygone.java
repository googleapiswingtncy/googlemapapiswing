package com.google.mapApi.map.addon.drawer.polygone;

import java.awt.Color;
import java.util.Collection;

import com.google.mapApi.map.addon.drawer.LatLngList;
import com.google.maps.model.LatLng;

public class Polygone extends LatLngList {

	private int id;
	private Color strokeColor;
	private double strokeOpacity;
	private int strokeWeight;
	private Color fillColor;
	private double fillOpacity;
	private boolean display = true;

	private static int nextId = 0;

	private static final long serialVersionUID = -9051109851848133820L;

	public Polygone(Color strokeColor, double strokeOpacity, int strokeWeight,Color fillColor, double fillOpacity, Collection<LatLng> c) {
		super();
		if (c != null && !c.isEmpty())
			addAll(c);
		setId(nextId);
		nextId++;
		setStrokeColor(strokeColor);
		setStrokeOpacity(strokeOpacity);
		setStrokeWeight(strokeWeight);
		setFillColor(fillColor);
		setFillOpacity(fillOpacity);
	}

	public Polygone(Collection<LatLng> c) {
		this(Color.RED, 0.6, 1,Color.RED,0.35, c);
	}

	public Polygone() {
		this(null);
	}

	public String getDefinition() {
		StringBuilder sb = new StringBuilder();
		sb.append("var flightPath = new google.maps.Polygon({\n");
		sb.append(" path : " + super.toString() + ",\n");
		sb.append(" strokeColor : '#" + Integer.toHexString(strokeColor.getRGB()).substring(2) + "',\n");
		sb.append(" strokeOpacity : " + strokeOpacity + ",\n");
		sb.append(" strokeWeight : " + strokeWeight + ",\n");
		sb.append(" fillColor : '#" + Integer.toHexString(fillColor.getRGB()).substring(2) + "',\n");
		sb.append(" fillOpacity : " + fillOpacity + "\n");
		sb.append("});\n");
		sb.append("flightPath.setMap(map);\n");
		return sb.toString();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("points : " + super.toString() + ",");
		sb.append("StrokeColor : '#" + Integer.toHexString(strokeColor.getRGB()).substring(2) + "',");
		sb.append("StrokeOpacity : " + strokeOpacity + ",");
		sb.append("Weight : " + strokeWeight + ", id : " + id + ",");
		sb.append("FillColor : '#" + Integer.toHexString(fillColor.getRGB()).substring(2) + "',");
		sb.append("FillOpacity : " + fillOpacity);
		return sb.toString();
	}

	/**
	 * @return the Id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	private void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the strokeColor
	 */
	public Color getStrokeColor() {
		return strokeColor;
	}

	/**
	 * @param strokeColor
	 *            the strokeColor to set
	 */
	public void setStrokeColor(Color strokeColor) {
		this.strokeColor = strokeColor;
	}

	/**
	 * @return the strokeOpacity
	 */
	public double getStrokeOpacity() {
		return strokeOpacity;
	}

	/**
	 * @param strokeOpacity
	 *            the strokeOpacity to set
	 */
	public void setStrokeOpacity(double strokeOpacity) {
		this.strokeOpacity = strokeOpacity;
	}

	/**
	 * @return the strokeWeight
	 */
	public int getStrokeWeight() {
		return strokeWeight;
	}

	/**
	 * @param strokeWeight
	 *            the strokeWeight to set
	 */
	public void setStrokeWeight(int strokeWeight) {
		this.strokeWeight = strokeWeight;
	}

	public void show() {
		this.setDisplay(true);
	}

	public void hide() {
		this.setDisplay(false);
	}

	public boolean isDisplay() {
		return display;
	}

	private void setDisplay(boolean display) {
		this.display = display;
	}

	public Color getFillColor() {
		return fillColor;
	}

	public void setFillColor(Color fillColor) {
		this.fillColor = fillColor;
	}

	public double getFillOpacity() {
		return fillOpacity;
	}

	public void setFillOpacity(double fillOpacity) {
		this.fillOpacity = fillOpacity;
	}

}
