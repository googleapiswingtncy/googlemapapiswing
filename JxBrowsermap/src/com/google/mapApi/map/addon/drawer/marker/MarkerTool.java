package com.google.mapApi.map.addon.drawer.marker;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.text.PlainDocument;

import com.google.mapApi.map.Map;
import com.google.mapApi.map.addon.MapControler;
import com.google.mapApi.map.context.GoogleApiContext;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

/**
 * Classe mettant en place l'outil de getion de marqueur integrer
 * 
 * This project and all these classes can be used without restriction for open
 * source or academic applications as part of projects with pedagogical purpose.
 * It remains the full property of its contributors and any use by enterprises
 * or companies need a written agreement between the contributors and the said
 * company.
 * 
 * @author Arnault Le Pr�vost-Corvellec
 *
 */
public class MarkerTool extends MapControler {

	private JTextField adr;
	private JTextField letter;
	private MarkerListAbstractTable mlat;

	/**
	 * constructeur
	 * 
	 * @param map
	 */
	public MarkerTool(Map map) {
		super(map);
	}

	@Override
	public JPanel getPanel() {
		JPanel out = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5, 0);
		c.fill = GridBagConstraints.BOTH;
		out.add(new JLabel("Position :"), c);

		adr = new JTextField(20);
		c.insets = new Insets(0, 0, 0, 0);
		c.gridx++;
		out.add(adr, c);

		c.gridx++;
		c.insets = new Insets(5, 10, 5, 0);
		out.add(new JLabel("Label :"), c);

		letter = new JTextField(2);
		PlainDocument doc = (PlainDocument) letter.getDocument();
		doc.setDocumentFilter(new OnlyOneCharDocumentFilter());
		c.insets = new Insets(0, 0, 0, 0);
		c.gridx++;
		out.add(letter, c);

		JButton addmarker = new JButton(getAddMarkerButtonText());
		addmarker.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GeoApiContext gc = GoogleApiContext.getContext();

				GeocodingResult[] results;
				try {
					if(adr.getText().trim().isEmpty())return;
					results = GeocodingApi.newRequest(gc).address(adr.getText().trim()).await();
					LatLng loc = results[0].geometry.location;
					LatLng ne = results[0].geometry.viewport.northeast;
					LatLng sw = results[0].geometry.viewport.southwest;
					Marker m = new Marker(loc, letter.getText().trim(), ne, sw);
					map.addMarker(m);
				} catch (Exception e1) {
					e1.printStackTrace();
				}

			}
		});
		c.gridx = 0;
		c.gridy++;
		c.gridwidth = 4;
		out.add(addmarker, c);

		mlat = new MarkerListAbstractTable(map.getMarkerList());
		JTable table = new JTable(mlat);
		table.getColumnModel().getColumn(0).setMaxWidth(60);
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(2).setMaxWidth(50);
		table.getColumnModel().getColumn(2).setResizable(false);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		c.gridy++;
		c.gridheight = 100;
		c.weighty = 1;
		out.add(new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER),
				c);

		JPanel buttons = new JPanel(new GridLayout(3, 2));

		JButton deleteOne = new JButton(getDeleteOneButtonText());
		deleteOne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = table.getSelectedRow();
				if (i < 0)
					return;
				map.deleteMarker(i);
			}
		});
		deleteOne.setMargin(new Insets(8, 5, 8, 5));
		buttons.add(deleteOne, 0);

		JButton deleteAll = new JButton(getDeleteAllButtonText());
		deleteAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				map.deleteMarkers();
			}
		});
		buttons.add(deleteAll, 1);

		JButton hideOne = new JButton(getHideOneButtonText());
		hideOne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = table.getSelectedRow();
				if (i < 0)
					return;
				map.hideMarker(i);
				table.setRowSelectionInterval(i, i);
			}
		});
		hideOne.setMargin(new Insets(8, 5, 8, 5));
		buttons.add(hideOne, 2);

		JButton hideAll = new JButton(getHideAllButtonText());
		hideAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = table.getSelectedRow();
				map.hideMarkers();
				if (i < 0)
					return;
				table.setRowSelectionInterval(i, i);
			}
		});
		buttons.add(hideAll, 3);

		JButton showOne = new JButton(getShowOneButtonText());
		showOne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = table.getSelectedRow();
				if (i < 0)
					return;
				map.showMarker(table.getSelectedRow());
				table.setRowSelectionInterval(i, i);
			}
		});
		showOne.setMargin(new Insets(8, 5, 8, 5));
		buttons.add(showOne, 4);

		JButton showAll = new JButton(getShowAllButtonText());
		showAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = table.getSelectedRow();
				map.showMarkers();
				if (i < 0)
					return;
				table.setRowSelectionInterval(i, i);
			}
		});
		buttons.add(showAll, 5);

		c.gridy += 100;
		c.gridheight = 1;
		c.weighty = 0;
		out.add(buttons, c);

		JButton fitMarkers = new JButton(getFitMarkerButtonText());
		fitMarkers.setMargin(new Insets(8, 5, 8, 5));
		fitMarkers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = table.getSelectedRow();
				if(map.getMarkerList().isEmpty())return;
				map.fitBoundsToMarkerList();
				if (i < 0)
					return;
				table.setRowSelectionInterval(i, i);
			}
		});
		c.gridy++;
		out.add(fitMarkers, c);

		return out;
	}

	private String getFitMarkerButtonText() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Ajuster l'affichage pour afficher tous les marqueurs visibles";
		default:
			return "Fit display to show all visible markers";
		}
	}

	private String getShowAllButtonText() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Afficher tous les marqueurs";
		default:
			return "Display all markers";
		}
	}

	private String getShowOneButtonText() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "<html>Afficher le marqueur selectionn&eacute</html>";
		default:
			return "Display selected marker";
		}
	}

	private String getHideAllButtonText() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Cacher tous les marqueurs";
		default:
			return "Hide all markers";
		}
	}

	private String getHideOneButtonText() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "<html>Cacher le marqueur selectionn&eacute</html>";
		default:
			return "Hide selected marker";
		}
	}

	private String getDeleteAllButtonText() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Supprimer tous les marqueurs";
		default:
			return "Delete all markers";
		}
	}

	private String getDeleteOneButtonText() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "<html>Supprimer le marqueur selectionn&eacute</html>";
		default:
			return "Delete selected marker";
		}
	}

	private String getAddMarkerButtonText() {
		switch (Locale.getDefault().getLanguage()) {
		case "fr":
			return "Ajouter ce marqueur";
		default:
			return "Add this marker";
		}
	}

	/**
	 * Methode de mise a jour pour la MarkerListAbstractTable
	 */
	public void update() {
		if (mlat != null)
			mlat.update();
	}
}
