package com.google.mapApi.map.addon.drawer.marker.icon;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * 
 * @author Arnault Le Pr�vost-Corvellec
 * https://sites.google.com/site/gmapsdevelopment/
 */
public class MarkerIcon implements Cloneable{
	private String path;
	public MarkerIcon(URL url) {
		path= url.toString();
	}
	public String toString() {
		return path;
	}
	public Object clone() throws CloneNotSupportedException {		
		try {
			return new MarkerIcon(new URL(path));
		} catch (MalformedURLException e) {
			return null;
		}
	}
}
