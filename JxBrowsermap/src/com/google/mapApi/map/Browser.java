package com.google.mapApi.map;

import static javafx.concurrent.Worker.State.FAILED;

import java.awt.BorderLayout;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.google.mapApi.tool.DebugOutput;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.embed.swing.JFXPanel;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebEvent;
import javafx.scene.web.WebView;

/**
 * A simple web browser 
 * 
 * This project and all these classes can be used without restriction for open
 * source or academic applications as part of projects with pedagogical purpose.
 * It remains the full property of its contributors and any use by enterprises
 * or companies need a written agreement between the contributors and the said
 * company.
 * 
 * @author Arnault Le Pr�vost-Corvellec
 *
 */
@SuppressWarnings("restriction")
public class Browser extends JFXPanel {

	private static final long serialVersionUID = -1912125019820728240L;

	private WebEngine engine;

	private final JPanel panel = new JPanel(new BorderLayout());
	private final JLabel lblStatus = new JLabel();

	public Browser() {
		super();
		createScene();
	}

	private void createScene() {

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				WebView view = new WebView();
				engine = view.getEngine();
				engine.setOnStatusChanged(new EventHandler<WebEvent<String>>() {
					@Override
					public void handle(final WebEvent<String> event) {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								lblStatus.setText(event.getData());
							}
						});
					}
				});
				engine.getLoadWorker().exceptionProperty().addListener(new ChangeListener<Throwable>() {
					public void changed(ObservableValue<? extends Throwable> o, Throwable old, final Throwable value) {
						if (engine.getLoadWorker().getState() == FAILED) {
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									JOptionPane.showMessageDialog(panel,
											(value != null) ? engine.getLocation() + "\n" + value.getMessage()
													: engine.getLocation() + "\nUnexpected error.",
											"Loading error...", JOptionPane.ERROR_MESSAGE);
								}
							});
						}
					}
				});
				setScene(new Scene(view));
			}
		});
	}

	public void loadHTML(String html) {
		Platform.runLater(new Runnable() {
			public void run() {
				engine.loadContent(html);
				;
			}
		});
	}

	private StringBuilder stackedCmd = new StringBuilder();

	public void executeJavaScript(String script) {
		Platform.runLater(new Runnable() {
			public void run() {
				synchronized (stackedCmd) {
					stackedCmd.append(script+";\n");
					synchronized (engine) {
						if (engine.getLoadWorker().stateProperty().get() != State.SUCCEEDED) {
							engine.getLoadWorker().stateProperty().addListener(new ChangeListener<State>() {
								public void changed(@SuppressWarnings("rawtypes") ObservableValue ov, State oldState,
										State newState) {
									if (engine.getLoadWorker().stateProperty().get() != State.SUCCEEDED) {
										return;
									}
									String cmd = stackedCmd.toString();
									DebugOutput.println(cmd);
									stackedCmd = new StringBuilder();
									engine.executeScript(cmd);
									engine.getLoadWorker().stateProperty().removeListener(this);
								}
							});
						} else {
							String cmd = stackedCmd.toString();
							stackedCmd= new StringBuilder();
							engine.executeScript(cmd);
						}
					}
				}

			}
		});/**/
	}

	public BufferedImage getScreen() {
		 BufferedImage imageCompo = new BufferedImage(this.getWidth(),this.getHeight(),BufferedImage.TYPE_INT_RGB);
         Graphics2D g2 = (Graphics2D) imageCompo.getGraphics();
         this.paintComponent(g2);
         return imageCompo;
	}
}
