package com.google.mapApi.map;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import com.google.mapApi.map.addon.drawer.marker.Marker;
import com.google.mapApi.map.addon.drawer.marker.icon.MarkerIconFactory;
import com.google.mapApi.map.addon.drawer.polyline.Polyline;
//import org.tncyhelper.map.addon.markerTool.Marker;
//import org.tncyhelper.map.context.GoogleApiContext;
import com.google.mapApi.tool.DebugOutput;
import com.google.maps.model.Bounds;
import com.google.maps.model.LatLng;

public class Launcher {

	/**
	 * fonction de lancement de l'application de test
	 * 
	 * This project and all these classes can be used without restriction for
	 * open source or academic applications as part of projects with pedagogical
	 * purpose. It remains the full property of its contributors and any use by
	 * enterprises or companies need a written agreement between the
	 * contributors and the said company.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// Locale.setDefault(new Locale("en", "us")); // Passe l'API en anglais
		// si le parametre "en" different de fr par default le Locale depend de
		// la langue par default de la VM
		// GoogleApiContext.init("YOUR API KEY","yourPsw");
		// Map map = new Map("yourPsw"); //load avec mot de passe pour que
		// l'utilisateur n'est pas a le taper attention son utilisation supprime
		// toute l'efficacit� de la securisation de la cl� d'api

		Map map = null;
		try {
			map = new Map();
		} catch (Exception e) {
			System.exit(0);
		}
		Polyline polyline = new Polyline();
		polyline.setStrokeWeight(5);
		polyline.setStrokeOpacity(1);
		polyline.add(new LatLng(49.856614, 1.3522219));
		polyline.add(new LatLng(41.836699, 3.360054));
		polyline.add(new LatLng(41.836699, 8.360054));
		map.addPolyline(polyline);
		map.fitBoundsToPolyline(0);
		map.addMarkerTool();
		map.addImageSaverTool();
		/*Bounds b = new Bounds();
		b.northeast = new LatLng(1, 1);
		b.southwest = new LatLng(-1, -1);
		map.addMarker(new Marker(new LatLng(0, 0), "", b, MarkerIconFactory.getMiniIcon("gray"), "O,O"));
		map.fitBoundsToMarkerList();*/
		JFrame jf = new JFrame();
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setLayout(new BorderLayout());
		jf.add(map);
		jf.pack();
		jf.setVisible(true);
		DebugOutput.println("Affichage");
		

		/*
		 * while (true) { try { Thread.sleep(50); } catch (InterruptedException
		 * e) { e.printStackTrace(); } map.hidePolygone(0);; try {
		 * Thread.sleep(50); } catch (InterruptedException e) {
		 * e.printStackTrace(); } map.showPolygone(0); } /
		 **/

	}
}
