# # README

### Swing API for Google map ###

An API provides the core functionality of the Google Map API in JavaScript for easy integration into a swing desktop application.

### Set up ###

Download and compile the source by integrating maven dependencies in pom.xml
With Eclipse you simply import the Eclipse's project and it will build dependencies alone.

Attention to version 0.1 is required to hold a License JxBrowser available on their site to use an academic or open source.

This API requires an API key and activate for valid javascript API and Geocoder for more information, I refer you to the google site.

### Who are we? ###

Administrator: Arnault Le Prevost-CORVELLEC arnault.le-prevost-corvellec@my-lessons.net

Contributors: Arnault Le Prevost-CORVELLEC arnault.le-prevost-corvellec@my-lessons.net

### License ###

This project can be used without restriction for open source or academic applications as part of projects with pedagogical purpose.
It remains the full property of its contributors and any use by enterprises or companies need a written agreement between the contributors and the said company.